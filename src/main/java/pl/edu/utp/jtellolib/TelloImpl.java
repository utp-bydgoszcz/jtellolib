package pl.edu.utp.jtellolib;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Implementation of Tello interface
 */
public class TelloImpl implements Tello {

	private final String name;
	private final String ipAddress;
	private final DronesManager dronesManager;
	private volatile String lastResponse = null;
	private volatile String lastResult = null;
	private volatile Integer lastValue = null;
	private volatile String lastSentControl = null;

	public TelloImpl(String name, String ipAddress, DronesManager swarmManager) {
		this.name = name;
		this.ipAddress = ipAddress;
		this.dronesManager = swarmManager;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void send(String command) {
		lastResponse = null;
		dronesManager.send(ipAddress, command);
		dronesManager.fireCommandSent(name, command.trim());
	}

	@Override
	public void sendControl(String command) {
		lastResult = null;
		lastSentControl = command;
		send(command);
	}

	@Override
	public void sendRead(String command) {
		lastValue = null;
		send(command);
	}

	@Override
	public String getLastResponse() {
		return lastResponse;
	}

	@Override
	public String getLastResult() {
		return lastResult;
	}

	@Override
	public Integer getLastValue() {
		return lastValue;
	}

	@Override
	public String getLastSentControl() {
		return lastSentControl;
	}

	@Override
	public DronesManager getDronesManager() {
		return dronesManager;
	}

	public void status(String status) {
		dronesManager.fireStatusChanged(name, status.trim());
		dronesManager.fireStatusChanged(name, parseStatus(status));
	}

	public void response(String response) {
		lastResponse = response;

		if (response.trim().equals("ok") || response.startsWith("error")) {
			lastResult = response;
		} else if (Utils.isNumber(response)) {
			lastValue = Integer.parseInt(response);
		}

		dronesManager.fireResponseReceived(name, response.trim());
	}

	@Override
	public String toString() {
		return name;
	}

	private static Map<String, String> parseStatus(String status) {
		return Arrays
				.stream(status.trim().split(";"))
				.map(v -> v.split(":"))
				.collect(Collectors.toMap(v -> v[0], v -> v[1]));
	}

}
