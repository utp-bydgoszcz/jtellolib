package pl.edu.utp.jtellolib;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Drone communications manager
 */
public class DronesManagerImpl implements DronesManager {

	private final DatagramSocket commandSocket;
	private final DatagramSocket statusSocket;
	private final Map<String, TelloImpl> tellos = new HashMap<>();
	private final Timer timer = new Timer(true);
	private final List<DronesListener> dronesListeners = new CopyOnWriteArrayList<>();

	/**
	 * Opens ports for communicate with Tello(s).
	 */
	public DronesManagerImpl() {
		try {
			// TODO closing connections on exit
			this.commandSocket = new DatagramSocket(8889);
			this.statusSocket = new DatagramSocket(8890);
			startResponseThread();
			startStatusThread();
			startTimer();
		} catch (SocketException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Connect to Tello drone
	 *
	 * @param name drone unique name
	 * @param ipAddress drone IP address
	 * @return Tello object
	 */
	@Override
	public Tello connect(String name, String ipAddress) {
		Tello tello = create(name, ipAddress);
		tello.command();
		tello.sync(2000);
		return tello;
	}

	/**
	 * Connect to Tello Edu drone, and enable mission pads support
	 *
	 * @param name drone unique name
	 * @param ipAddress drone IP address
	 * @return Tello object
	 */
	@Override
	public Tello connectEdu(String name, String ipAddress) {
		Tello tello = create(name, ipAddress);
		tello.command();
		tello.sync(2000);
		tello.mon();
		tello.sync(2000);
		tello.mdirection(0);
		tello.sync(2000);
		return tello;
	}

	/**
	 * Connects to Tello drone using defaults
	 *
	 * @return
	 */
	@Override
	public Tello connect() {
		return connect("Tello", Tello.DEFAULT_IP_ADDRESS);
	}

	/**
	 *
	 * @param address
	 * @param command
	 */
	@Override
	public void send(String address, String command) {

		if (tellos.containsKey(address)) {
			try {
				DatagramPacket packet = new DatagramPacket(command.getBytes(), command.getBytes().length, InetAddress.getByName(address), 8889);
				commandSocket.send(packet);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void close() {
		try {
			commandSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			statusSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void fireStatusChanged(String droneName, Map<String, String> statusValues) {
		dronesListeners.forEach(dl -> dl.statusChanged(droneName, statusValues));
	}

	@Override
	public void fireStatusChanged(String droneName, String rawStatus) {
		dronesListeners.forEach(dl -> dl.statusChanged(droneName, rawStatus));
	}

	@Override
	public void fireResponseReceived(String droneName, String response) {
		dronesListeners.forEach(dl -> dl.responseReceived(droneName, response));
	}

	@Override
	public void fireCommandSent(String droneName, String command) {
		dronesListeners.forEach(dl -> dl.commandSent(droneName, command));
	}

	@Override
	public void publishInfo(String text) {
		dronesListeners.forEach(dl -> dl.info(text));
	}

	@Override
	public void addDronesListener(DronesListener dronesListener) {
		dronesListeners.add(dronesListener);
	}

	@Override
	public void removeDronesListener(DronesListener dronesListener) {
		dronesListeners.remove(dronesListener);
	}

	private Tello create(String name, String address) {
		TelloImpl tello = new TelloImpl(name, address, this);
		tellos.put(address, tello);
		return tello;
	}

	private void startResponseThread() {
		Thread t = new Thread(() -> {
			try {
				for (;;) {
					receiveResponse();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		t.setDaemon(true);
		t.start();
	}

	private void receiveResponse() throws IOException {
		byte[] buf = new byte[1024];
		DatagramPacket packet = new DatagramPacket(buf, buf.length);
		commandSocket.receive(packet);

		byte[] buf2 = new byte[packet.getLength()];
		System.arraycopy(buf, 0, buf2, 0, packet.getLength());
		String message = new String(buf2);

		if (tellos.containsKey(packet.getAddress().getHostAddress())) {
			tellos.get(packet.getAddress().getHostAddress()).response(message);
		}
	}

	private void startStatusThread() {
		Thread t = new Thread(() -> {
			try {
				for (;;) {
					receiveStatus();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		t.setDaemon(true);
		t.start();
	}

	private void startTimer() {
		TimerTask tt = new TimerTask() {
			@Override
			public void run() {
				tellos.forEach((String addr, Tello tello) -> tello.wifi());
			}
		};
		timer.scheduleAtFixedRate(tt, 3000, 3000);
	}

	private void receiveStatus() throws IOException {
		byte[] buf = new byte[1024];
		DatagramPacket packet = new DatagramPacket(buf, buf.length);
		statusSocket.receive(packet);

		byte[] buf2 = new byte[packet.getLength()];
		System.arraycopy(buf, 0, buf2, 0, packet.getLength());
		String message = new String(buf2);

		if (tellos.containsKey(packet.getAddress().getHostAddress())) {
			tellos.get(packet.getAddress().getHostAddress()).status(message);
		}
	}

}
