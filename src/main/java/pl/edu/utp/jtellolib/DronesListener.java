package pl.edu.utp.jtellolib;

import java.util.Map;

/**
 * Interface for drones statuses and responses
 */
public interface DronesListener {

	/**
	 * Method call when drone sent new status
	 * @param droneName
	 * @param statusValues status data
	 */
	default void statusChanged(String droneName, Map<String, String> statusValues) {
	}

	/**
	 * Method call when drone sent new status
	 * @param droneName
	 * @param rawStatus 
	 */
	default void statusChanged(String droneName, String rawStatus) {
	}
	
	/**
	 * Method call when drone dent response
	 * @param droneName
	 * @param response 
	 */
	default void responseReceived(String droneName, String response) {
	}
	
	/**
	 * method call when command is sent to drone
	 * @param droneName
	 * @param command 
	 */
	default void commandSent(String droneName, String command) {
	}
	
	/**
	 * Publish info message
	 * @param message  
	 */
	default void info(String message) {
		
	}

}
