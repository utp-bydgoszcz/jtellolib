package pl.edu.utp.jtellolib;

import java.util.Map;

/**
 * Drone communications manager
 */
public interface DronesManager extends AutoCloseable {

	/**
	 * Connect to Tello drone
	 *
	 * @param name drone unique name
	 * @param ipAddress drone IP address
	 * @return Tello object
	 */
	Tello connect(String name, String ipAddress);

	/**
	 * Connect to Tello Edu drone, and enable mission pads support
	 *
	 * @param name drone unique name
	 * @param ipAddress drone IP address
	 * @return Tello object
	 */
	Tello connectEdu(String name, String ipAddress);

	/**
	 * Connects to Tello drone using defaults
	 *
	 * @return
	 */
	Tello connect();

	void send(String address, String command);

	void fireStatusChanged(String droneName, Map<String, String> statusValues);

	void fireStatusChanged(String droneName, String rawStatus);

	void fireResponseReceived(String droneName, String response);

	void fireCommandSent(String droneName, String command);

	void publishInfo(String text);

	void addDronesListener(DronesListener dronesListener);

	void removeDronesListener(DronesListener dronesListener);

	static DronesManager createPureManager() {
		return new DronesManagerImpl();
	}

	static DronesManager createManager() {
		DronesManager dm = new DronesManagerImpl();
		dm.addDronesListener(new ConsoleLogger());
		return dm;
	}

}
