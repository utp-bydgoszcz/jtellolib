package pl.edu.utp.jtellolib;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConsoleLogger implements DronesListener {

	private final long startTime = System.currentTimeMillis();
	private final boolean formatedMode;
	private final boolean fastMode;
	private final Map<String, Integer> statusCounters = new ConcurrentHashMap<>();

	public ConsoleLogger() {
		this.formatedMode = true;
		this.fastMode = false;
	}

	/**
	 *
	 * @param formatedMode statuses will be formated
	 * @param fastMode true = 2 Hz, false = 0.5 Hz
	 */
	public ConsoleLogger(boolean formatedMode, boolean fastMode) {
		this.formatedMode = formatedMode;
		this.fastMode = fastMode;
	}

	@Override
	public void statusChanged(String droneName, Map<String, String> statusValues) {
		if (!formatedMode) {
			int statusCounter = statusCounters.containsKey(droneName) ? statusCounters.get(droneName) : 0;
			int divider = fastMode ? 5 : 20;

			if (statusCounter % divider == 0) {
					System.out.println(String.format("%s [%s]: %s", droneName, Utils.formatedFlightTime(getFlightTime()), formatStatus(statusValues)));
			}

			statusCounters.put(droneName, statusCounter);
		}
	}

	@Override
	public void statusChanged(String droneName, String rawStatus) {
		if (!formatedMode) {
			int statusCounter = statusCounters.containsKey(droneName) ? statusCounters.get(droneName) : 0;
			int divider = fastMode ? 5 : 20;

			if (statusCounter % divider == 0) {
				System.out.println(String.format("[%s] %s: %s", Utils.formatedFlightTime(getFlightTime()), droneName, rawStatus));
			}

			statusCounters.put(droneName, statusCounter);
		}
	}

	@Override
	public void responseReceived(String droneName, String response) {
		System.out.println(String.format("[%s] %s: %s", Utils.formatedFlightTime(getFlightTime()), droneName, response));
	}

	@Override
	public void commandSent(String droneName, String command) {
		System.out.println(String.format("[%s] %s sent: %s", Utils.formatedFlightTime(getFlightTime()), droneName, command));
	}

	@Override
	public void info(String text) {
		System.out.println(String.format("[%s] info: %s", Utils.formatedFlightTime(getFlightTime()), text));
	}

	
	
	public long getFlightTime() {
		return System.currentTimeMillis() - startTime;
	}

	private static String formatStatus(Map<String, String> values) {
		// if Tello Edu
		if (values.containsKey("mid")) {
			return String.format(
					"bat: %s mid: %s x: %s y: %s z: %s h: %s yaw: %s temp: %s time: %s",
					values.get("bat"),
					values.get("mid"),
					values.get("x"),
					values.get("y"),
					values.get("z"),
					values.get("h"),
					values.get("yaw"),
					values.get("temph"),
					values.get("time")
			);
		} else {
			return String.format(
					"bat: %s h: %s yaw: %s temp: %s time: %s",
					values.get("bat"),
					values.get("h"),
					values.get("yaw"),
					values.get("temph"),
					values.get("time")
			);
		}
	}
}
