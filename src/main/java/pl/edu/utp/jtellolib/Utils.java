package pl.edu.utp.jtellolib;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility class
 */
public final class Utils {

	/**
	 * Waits given time
	 *
	 * @param milis miliseconds
	 */
	public static void sleep(long milis) {
		try {
			Thread.sleep(milis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Waits untill all given drones finish operation, bun not longer then
	 * default timeout
	 *
	 * @param drones drones
	 */
	public static void sync(Tello... drones) {
		sync(Tello.DEFAULT_SYNC_TIMEOUT, drones);
	}

	/**
	 * Waits untill all given drones finish operation, bun not longer then
	 * timeout
	 *
	 * @param timeout timeout in miliseconds
	 * @param drones drones
	 */
	public static void sync(long timeout, Tello... drones) {
		if (timeout <= 0) {
			throw new IllegalArgumentException("Timeout must be positive");
		}
		long startTime = System.currentTimeMillis();
		// drones without response
		List<Tello> dronesForWitchWaits = Arrays
				.stream(drones)
				.filter(d -> !d.readyForControl())
				.collect(Collectors.toList());

		// waiting not longer then timeout
		while (System.currentTimeMillis() - startTime < timeout && !dronesForWitchWaits.isEmpty()) {
			dronesForWitchWaits = dronesForWitchWaits
					.stream()
					.filter(d -> !d.readyForControl())
					.collect(Collectors.toList());
			Utils.sleep(50);
		}

		// if any drone has timeout
		if (!dronesForWitchWaits.isEmpty()) {
			String message = String.format("!!! %s sync timeout (>%s ms)", dronesForWitchWaits.stream().map(t -> t.getName()).collect(Collectors.joining()), timeout);
			dronesForWitchWaits.stream().findAny().ifPresent((Tello t) -> t.getDronesManager().publishInfo(message));
		}
	}

	/**
	 * Applays operation (command) to all drones of swarm
	 *
	 * @param operation operation
	 * @param drones drones
	 */
	public static void applyAll(Consumer<Tello> operation, Tello... drones) {
		Arrays.stream(drones)
				.forEach(operation);
	}

	/**
	 * Applys operiation to all drones, and wait until done using default
	 * timeout
	 *
	 * @param operation operation
	 * @param drones drones
	 */
	public static void applyAndSync(Consumer<Tello> operation, Tello... drones) {
		applyAll(operation, drones);
		sync(drones);
	}

	/**
	 * Applys operiation to all drones, and wait until done
	 *
	 * @param operation operation
	 * @param timeout timeout in miliseconds
	 * @param drones drones
	 */
	public static void applyAndSync(Consumer<Tello> operation, long timeout, Tello... drones) {
		applyAll(operation, drones);
		sync(timeout, drones);
	}

	public static String formatedFlightTime(long flightTime) {
		return String.format("%d.%d", flightTime / 1000, flightTime % 1000 / 100);
	}

	public static boolean isNumber(String string) {
		try {
			Integer.parseInt(string);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
}
